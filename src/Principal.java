import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class Principal extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField Edadtxt;
    private JTextField Nombretxt;
    private JTable table1;
    private JButton adicionarButton;
    private JButton eliminarButton;
    private DefaultTableModel model;  // se genera la variable model

    public Principal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocation(350,50);


        model = new DefaultTableModel();
        model.addColumn("nombre");
        model.addColumn("edad");
        table1.setModel(model);




        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre= Nombretxt.getText();
                int edad = Integer.parseInt(Edadtxt.getText());

                Nombretxt.setText("");
                Edadtxt.setText("");

                model.addRow(new Object[]{nombre,edad});

                JOptionPane.showConfirmDialog(getParent(),"ueted es: "+nombre+" y su edad"+edad);
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
    }

    private void onOK() {
        JOptionPane.showMessageDialog(null,"hola mundo");
        // add your code here
        dispose();//JOptionPane.showMessageDialog(null,"hola mundo");
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Principal dialog = new Principal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
